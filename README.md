# kera-flink-connector

This repository implements a connector to read [KerA](https://kerdata.gitlabpages.inria.fr/Kerdata-Codes/kera-website/) streams with [Apache Flink](http://flink.apache.org/) stream processing framework.

## Compilation

The connector requires the `kera-java` dependency, which should be added to the local Maven repository:

```bash
mvn install:install-file -Dfile=<bindings-path> -DgroupId=fr.inria.kera -DartifactId=kera-java -Dversion=1.0.0 -Dpackaging=jar -DgeneratePom=true
```

Replace `<bindings-path>` by the path to the Java KerA bindings, typically `kera/bindings/java/kera_jni/build/libs/kera_jni.jar` or `kera/install/lib/kera/kera_jni.jar`.

Compile the project as follows:

```bash
mvn clean package -DskipTests -Dcheckstyle.skip=true
```

Add the output JAR to the local Maven repository as follows:

```bash
mvn install:install-file -Dfile=target/kera-flink-connector-1.0.0.jar -DgroupId=fr.inria.kera -DartifactId=kera-flink-connector -Dversion=1.0.0 -Dpackaging=jar -DgeneratePom=true
```
