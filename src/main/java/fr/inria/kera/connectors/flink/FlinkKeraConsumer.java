/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.inria.kera.connectors.flink;

import fr.inria.kera.KeraBindingsSingleThread;
import fr.inria.kera.Streamlet;
import fr.inria.kera.Handover;
import org.apache.flink.streaming.api.operators.StreamingRuntimeContext;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;
import org.apache.flink.streaming.connectors.kafka.internals.KafkaDeserializationSchemaWrapper;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ArrayList;
import java.util.Set;

import static org.apache.flink.util.Preconditions.checkNotNull;

public class FlinkKeraConsumer<T> extends FlinkKeraConsumerBase<T> {
	private static final long serialVersionUID = -1L;

	/**
	 * User-supplied properties
	 **/
	protected final Properties properties;

	public FlinkKeraConsumer(String topic, DeserializationSchema<T> deserializer, Properties props) {
		this(Collections.singletonList(topic), new KafkaDeserializationSchemaWrapper<>(deserializer), props);
	}

	public FlinkKeraConsumer(String topic, KafkaDeserializationSchema<T> deserializer, Properties props) {
		this(Collections.singletonList(topic), deserializer, props);
	}

	public FlinkKeraConsumer(List<String> topics, KafkaDeserializationSchema<T> deserializer, Properties props) {
		super(topics, deserializer);

		this.properties = checkNotNull(props, "props");
		//setDeserializer(this.properties);
	}

	@Override
	protected AbstractKeraFetcher<T, ?> createFetcher(
			SourceContext<T> sourceContext,
			Map<KeraTopicStreamletGroup,KeraTopicStreamletGroup.Segment> subscribedPartitionsToStartOffsets,
			StreamingRuntimeContext runtimeContext) throws Exception
	{
		List<Streamlet> streamlets =  new ArrayList<Streamlet>();

		for (KeraTopicStreamletGroup sg : (Set<KeraTopicStreamletGroup>)subscribedPartitionsToStartOffsets.keySet()) {
			streamlets.add(new Streamlet(sg.getTopic(), sg.getStreamlet()));
		}

		return new KeraFetcher<T>(streamlets, this.properties, this.deserializer,
				sourceContext, subscribedPartitionsToStartOffsets, runtimeContext.getUserCodeClassLoader());
	}

	@Override
	protected List<KeraTopicStreamletGroup> getKeraStreamlets(
			List<String> topics,
			int numberTaskManagers,
			int numberNodes,
			int indexOfThisSubtask,
			int numParallelSubtasks) {
		List<KeraTopicStreamletGroup> sgList = new ArrayList<>();
		
		if (numberTaskManagers == 0) {
			//assumes 1..NSTREAMLETS streamlet ids created
			int streamletCount = Integer.valueOf(this.properties.getProperty("NSTREAMLETS"));
			for (int i = 1; i <= streamletCount; i++) {
				sgList.add(new KeraTopicStreamletGroup((String) topics.get(0), i, KeraTopicStreamletGroup.INVALID_GROUP, true));
			}
		} else {
			KeraBindingsSingleThread keraBindings = new KeraBindingsSingleThread(this.properties.getProperty("locator"),
					this.properties.getProperty("clusterName"), new Handover());
			//get local streamlets
			//properties is updated with indexPartition that is used by fetcher
			List<Streamlet> streamlets = keraBindings.getLocalStreamlets(this.properties, numberTaskManagers, numberNodes, indexOfThisSubtask, numParallelSubtasks);
			
			for (int i = 0; i < streamlets.size(); i++) {
				Streamlet s = streamlets.get(i);
				sgList.add(new KeraTopicStreamletGroup(s.stream(), s.streamletId(), 
						KeraTopicStreamletGroup.INVALID_GROUP, s.isAssigned()));
			}
		}
		return sgList;
	}

//	private static void setDeserializer(Properties props) {
//		final String deSerName = ByteArrayDeserializer.class.getCanonicalName();
//
//		Object keyDeSer = props.get(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG);
//		Object valDeSer = props.get(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG);
//
//		if (keyDeSer != null && !keyDeSer.equals(deSerName)) {
//			LOG.warn("Ignoring configured key DeSerializer ({})", ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG);
//		}
//		if (valDeSer != null && !valDeSer.equals(deSerName)) {
//			LOG.warn("Ignoring configured value DeSerializer ({})", ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG);
//		}
//
//		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, deSerName);
//		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, deSerName);
//	}
}
