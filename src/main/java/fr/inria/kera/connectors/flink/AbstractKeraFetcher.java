/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera.connectors.flink;

import org.apache.flink.streaming.api.functions.source.SourceFunction;

import java.util.Map;

import static org.apache.flink.util.Preconditions.checkNotNull;

public abstract class AbstractKeraFetcher<T, KPH> {

	/**
	 * The source context to emit records and watermarks to
	 */
	protected final SourceFunction.SourceContext<T> sourceContext;

	/**
	 * The lock that guarantees that record emission and state updates are atomic,
	 * from the view of taking a checkpoint
	 */
	protected final Object checkpointLock;

	//todo subscribed streamlets states?

	protected AbstractKeraFetcher(
			SourceFunction.SourceContext<T> sourceContext,
			Map<KeraTopicStreamletGroup, KeraTopicStreamletGroup.Segment> subscribedPartitionsToStartOffsets,
			ClassLoader userCodeClassLoader) {
		this.sourceContext = checkNotNull(sourceContext);
		this.checkpointLock = sourceContext.getCheckpointLock();

	}

	// ------------------------------------------------------------------------
	//  Core fetcher work methods
	// ------------------------------------------------------------------------

	public abstract void runFetchLoop() throws Exception;

	public abstract void cancel();

	//todo should handle subscribed streamlet state
	protected void emitRecord(T record) throws Exception {
		synchronized (checkpointLock) {
			sourceContext.collect(record);
		}
	}
}
