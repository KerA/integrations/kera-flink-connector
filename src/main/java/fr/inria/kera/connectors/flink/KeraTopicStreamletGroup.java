/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera.connectors.flink;

import java.io.Serializable;

import static java.util.Objects.requireNonNull;

/**
 * Flink's description of a streamlet's group in a Kera topic.
 *
 * <p>Note: This class must not change in its structure, because it would change the
 * serialization format and make previous savepoints unreadable.
 */
public final class KeraTopicStreamletGroup implements Serializable {

	/**
	 * THIS SERIAL VERSION UID MUST NOT CHANGE, BECAUSE IT WOULD BREAK
	 * READING OLD SERIALIZED INSTANCES FROM SAVEPOINTS
	 */
	private static final long serialVersionUID = -1L;

	// ------------------------------------------------------------------------

	private final String topic;
	private final int streamlet;
	private final long group;
	private final int cachedHash;
	private final boolean assigned;

	public static final long INVALID_GROUP = -1L;
	public static final long INVALID_SEGMENT = -1L;
	public static final Segment invalidSegment = new KeraTopicStreamletGroup.Segment(INVALID_SEGMENT, 0);

	public KeraTopicStreamletGroup(String topic, int streamlet, long group, boolean assigned) {
		this.topic = requireNonNull(topic);
		this.streamlet = streamlet;
		this.group = group;
		this.assigned = assigned;
		this.cachedHash = 31 * topic.hashCode() + streamlet + (int) group;
	}

	public KeraTopicStreamletGroup(String topic, int streamlet, boolean assigned) {
		this.topic = requireNonNull(topic);
		this.streamlet = streamlet;
		this.group = INVALID_GROUP;
		this.assigned = assigned;
		this.cachedHash = 31 * topic.hashCode() + streamlet + (int) group;
	}

	// ------------------------------------------------------------------------

	public String getTopic() {
		return topic;
	}

	public int getStreamlet() {
		return streamlet;
	}

	public long getGroup() {
		return group;
	}

	public boolean isAssigned() {
		return assigned;
	}
	// ------------------------------------------------------------------------

	@Override
	public String toString() {
		return "KeraTopicStreamletGroup{" +
				"topic='" + topic + '\'' +
				", streamlet=" + streamlet +
				", group=" + group +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o instanceof KeraTopicStreamletGroup) {
			KeraTopicStreamletGroup that = (KeraTopicStreamletGroup) o;
			return this.streamlet == that.streamlet && this.group == that.group && this.topic.equals(that.topic);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return cachedHash;
	}

	public final static class Segment implements Serializable {
		private static final long serialVersionUID = -1L;

		private final long segment;
		private final int offset;

		public Segment(long segment, int offset) {
			this.segment = segment;
			this.offset = offset;
		}

		public long getSegment() {
			return this.segment;
		}

		public int getOffset() {
			return this.offset;
		}


	}
}
