/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera.connectors.flink;

import org.apache.commons.collections.map.LinkedMap;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.typeutils.ResultTypeQueryable;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.runtime.state.CheckpointListener;
import org.apache.flink.runtime.state.FunctionInitializationContext;
import org.apache.flink.runtime.state.FunctionSnapshotContext;
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction;
import org.apache.flink.streaming.api.functions.source.RichParallelSourceFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.operators.StreamingRuntimeContext;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;
import org.apache.flink.util.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.flink.util.Preconditions.checkArgument;
import static org.apache.flink.util.Preconditions.checkNotNull;

//todo running multiple consumer instances within the same taskManager is not safe
//preferable 1 consumer per taskManager
//this will create 1 KeraFetcher which should be synchronized for access, todo
//with 1 slot per TM, many TMs per node, all good

public abstract class FlinkKeraConsumerBase<T> extends RichParallelSourceFunction<T> implements
		CheckpointListener, ResultTypeQueryable<T>, CheckpointedFunction {

	private static final long serialVersionUID = -1L;

	protected static final Logger LOG = LoggerFactory.getLogger(FlinkKeraConsumerBase.class);

	/**
	 * The maximum number of pending non-committed checkpoints to track, to avoid memory leaks
	 */
	public static final int MAX_NUM_PENDING_CHECKPOINTS = 100;

	// ------------------------------------------------------------------------
	//  configuration state, set on the client relevant for all subtasks
	// ------------------------------------------------------------------------

	protected final List<String> topics;

	//get a list of Streamlet/s with getKeraStreamlets(topics)

	/**
	 * The schema to convert between Kera's byte messages, and Flink's objects
	 */
	protected final KafkaDeserializationSchema<T> deserializer;

	/**
	 * The set of topic streamlet groups that the source will read, with their initial segment's offsets to start reading from
	 */
	private Map<KeraTopicStreamletGroup, KeraTopicStreamletGroup.Segment> subscribedPartitionsToStartOffsets;

	private transient ListState<Tuple2<KeraTopicStreamletGroup, KeraTopicStreamletGroup.Segment>> offsetsStateForCheckpoint;

	/**
	 * User-set flag determining whether or not to commit on checkpoints.
	 * Note: this flag does not represent the final offset commit mode.
	 */
	private boolean enableCommitOnCheckpoints = true;

	// ------------------------------------------------------------------------
	//  runtime state (used individually by each parallel subtask)
	// ------------------------------------------------------------------------

	/**
	 * Data for pending but uncommitted offsets
	 */
	private final LinkedMap pendingOffsetsToCommit = new LinkedMap();

	/**
	 * The fetcher implements the connections to the Kera brokers
	 */
	private transient volatile AbstractKeraFetcher<T, ?> keraFetcher;

	/**
	 * The offsets to restore to, if the consumer restores state from a checkpoint
	 */
	private transient volatile HashMap<KeraTopicStreamletGroup, KeraTopicStreamletGroup.Segment> restoredState;

	/**
	 * Flag indicating whether the consumer is still running
	 **/
	private volatile boolean running = true;

	/**
	 * Whether this operator instance was restored from checkpointed state.
	 */
	private transient boolean restored = false;

	/**
	 * Base constructor.
	 *
	 * @param deserializer The deserializer to turn raw byte messages into Java/Scala objects.
	 */
	public FlinkKeraConsumerBase(List<String> topics, KafkaDeserializationSchema<T> deserializer) {
		this.topics = checkNotNull(topics);
		checkArgument(topics.size() > 0, "You have to define at least one topic.");
		this.deserializer = checkNotNull(deserializer, "valueDeserializer");
	}

	public FlinkKeraConsumerBase<T> setCommitOffsetsOnCheckpoints(boolean commitOnCheckpoints) {
		this.enableCommitOnCheckpoints = commitOnCheckpoints;
		return this;
	}

	public static int assign(KeraTopicStreamletGroup streamletGroup, int numParallelSubtasks) {
		int startIndex = ((streamletGroup.getTopic().hashCode() * 31) & 0x7FFFFFFF) % numParallelSubtasks;

		// here, the assumption is that the id of Kera streamlets are always ascending
		// starting from 1, and therefore can be used directly as the offset clockwise from the start index
		return (startIndex + streamletGroup.getStreamlet() - 1) % numParallelSubtasks;
	}

	@Override
	public void open(Configuration parameters) throws Exception {
		if (restored) {
			subscribedPartitionsToStartOffsets = restoredState;

			LOG.info("Consumer subtask {} will start reading {} partitions with offsets in restored state: {}",
					getRuntimeContext().getIndexOfThisSubtask(), subscribedPartitionsToStartOffsets.size(), subscribedPartitionsToStartOffsets);
		} else {
			System.out.println("not restored");

			// initialize subscribed partitions/streamlets
			int numberTaskManagers = 0; //unknown
			int numberNodes = 0;
			if (getRuntimeContext().getExecutionConfig().getGlobalJobParameters() != null) {
				System.out.println("inside");
				ParameterTool parameterTool = (ParameterTool)
	    			getRuntimeContext().getExecutionConfig().getGlobalJobParameters();
				numberTaskManagers = parameterTool.getInt("numberTaskManagers", 0);
				numberNodes = parameterTool.getInt("numberNodes", 0);

			}

			System.out.println("numberTaskManagers " + numberTaskManagers);
			System.out.println("numberNodes " + numberNodes);

			int indexOfThisSubtask = getRuntimeContext().getIndexOfThisSubtask();
			int numParallelSubtasks = getRuntimeContext().getNumberOfParallelSubtasks();

			List<KeraTopicStreamletGroup> keraTopicStreamlets = getKeraStreamlets(
					topics, numberTaskManagers, numberNodes, indexOfThisSubtask, numParallelSubtasks);
			Preconditions.checkNotNull(keraTopicStreamlets, "KeraTopicStreamletGroup must not be null.");

			System.out.println("keraTopicStreamlets length " + keraTopicStreamlets.size());

			// map streamlets to subscribedPartitionsToStartOffsets by subtask index
			//assign as is works for Flink separated from Kera

			subscribedPartitionsToStartOffsets = new HashMap<>(keraTopicStreamlets.size());

			if(numberTaskManagers == 0) { //assign based on task index and streamlet id
				for (KeraTopicStreamletGroup streamletGroup : keraTopicStreamlets) {
					if (assign(streamletGroup, numParallelSubtasks) == indexOfThisSubtask) {
						subscribedPartitionsToStartOffsets.put(streamletGroup, KeraTopicStreamletGroup.invalidSegment);
					}
				}
			} else { //use all local streamlets
				for (KeraTopicStreamletGroup streamletGroup : keraTopicStreamlets) {
					subscribedPartitionsToStartOffsets.put(streamletGroup, KeraTopicStreamletGroup.invalidSegment);
				}
			}

			System.out.println("subscribedPartitionsToStartOffsets length " + subscribedPartitionsToStartOffsets.size());

			if (subscribedPartitionsToStartOffsets.size() != 0) {
				LOG.info("Consumer subtask {} will start reading the following {} partitions from the earliest offsets: {}",
						getRuntimeContext().getIndexOfThisSubtask(),
						subscribedPartitionsToStartOffsets.size(),
						subscribedPartitionsToStartOffsets.keySet());
			}
		}
	}

	@Override
	public void run(SourceContext<T> sourceContext) throws Exception {
		System.out.println("------run");

		if (subscribedPartitionsToStartOffsets == null) {
			throw new Exception("The partitions were not set for the consumer");
		}

		// we need only do work, if we actually have partitions assigned
		if (!subscribedPartitionsToStartOffsets.isEmpty()) {
			System.out.println("subscribedPartitionsToStartOffsets is not empty");
			// create the fetcher that will communicate with the Kera brokers
			final AbstractKeraFetcher<T, ?> fetcher = createFetcher(sourceContext,
					subscribedPartitionsToStartOffsets,
					(StreamingRuntimeContext) getRuntimeContext());
			this.keraFetcher = fetcher;
			if (!running) {
				return;
			}
			fetcher.runFetchLoop();
		} else {
			System.out.println("subscribedPartitionsToStartOffsets is empty");

			// this source never completes, so emit a Long.MAX_VALUE watermark
			// to not block watermark forwarding
			sourceContext.emitWatermark(new Watermark(Long.MAX_VALUE));
			// wait until this is canceled
			final Object waitLock = new Object();
			while (running) {
				try {
					//noinspection SynchronizationOnLocalVariableOrMethodParameter
					synchronized (waitLock) {
						waitLock.wait();
					}
				} catch (InterruptedException e) {
					if (!running) {
						// restore the interrupted state, and fall through the loop
						Thread.currentThread().interrupt();
					}
				}
			}
		}
	}

	@Override
	public void cancel() {
		// set ourselves as not running
		running = false;

		// abort the fetcher, if there is one
		if (keraFetcher != null) {
			keraFetcher.cancel();
		}

		// there will be an interrupt() call to the main thread anyways
	}

	@Override
	public void close() throws Exception {
		// pretty much the same logic as cancelling
		try {
			cancel();
		} finally {
			super.close();
		}
	}

	@Override
	public final void initializeState(FunctionInitializationContext context) throws Exception {
		LOG.info("No restore state for FlinkKeraConsumer.");
	}

	@Override
	public final void snapshotState(FunctionSnapshotContext context) throws Exception {
		if (!running) {
			LOG.debug("snapshotState() called on closed source");
		} else {
			LOG.info("snapshotState() not implemented");
		}
	}

	@Override
	public final void notifyCheckpointComplete(long checkpointId) throws Exception {
		if (!running) {
			LOG.debug("notifyCheckpointComplete() called on closed source");
			return;
		}

		final AbstractKeraFetcher<?, ?> fetcher = this.keraFetcher;
		if (fetcher == null) {
			LOG.debug("notifyCheckpointComplete() called on uninitialized source");
			return;
		}
	}

	protected abstract AbstractKeraFetcher<T, ?> createFetcher(
			SourceFunction.SourceContext<T> sourceContext,
			Map<KeraTopicStreamletGroup, KeraTopicStreamletGroup.Segment> subscribedPartitionsToStartOffsets,
			StreamingRuntimeContext runtimeContext) throws Exception;

	protected abstract List<KeraTopicStreamletGroup> getKeraStreamlets(
			List<String> topics, int numberTaskManagers, int numberNodes, int indexOfThisSubtask, int numParallelSubtasks);

	// ------------------------------------------------------------------------
	//  ResultTypeQueryable methods
	// ------------------------------------------------------------------------

	@Override
	public TypeInformation<T> getProducedType() {
		return deserializer.getProducedType();
	}

}
