/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera.connectors.flink;

import fr.inria.kera.ConsumerRecords;
import fr.inria.kera.ConsumerRecord;
import fr.inria.kera.Handover;
import fr.inria.kera.KeraBindingsSingleThread;
import fr.inria.kera.KeraHandoverPullSingleThread;
import fr.inria.kera.Streamlet;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;

import java.util.List;
import java.util.Map;
import java.util.Properties;

public class KeraFetcher<T> extends AbstractKeraFetcher<T, Streamlet> {

	/**
	 * The schema to convert between Kera's byte messages, and Flink's objects
	 */
	private final KafkaDeserializationSchema<T> deserializer;

	/**
	 * The handover of data and exceptions between the consumer thread and the task thread
	 */
	private final Handover handover;
	private final Properties keraProperties;
	private final List<Streamlet> subscribedStreamlets;

	/**
	 * Flag to mark the main work loop as alive
	 */
	private volatile boolean running = true;

	private final KeraHandoverPullSingleThread consumerThread;

	private final KeraBindingsSingleThread keraBindings;

	public KeraFetcher(List<Streamlet> subscribedStreamlets, Properties keraProperties, 
			KafkaDeserializationSchema<T> deserializer, SourceFunction.SourceContext<T> sourceContext, 
			Map<KeraTopicStreamletGroup, KeraTopicStreamletGroup.Segment> subscribedPartitionsToStartOffsets, 
			ClassLoader userCodeClassLoader) {
		super(sourceContext, subscribedPartitionsToStartOffsets, userCodeClassLoader);

		this.deserializer = deserializer;
		this.handover = new Handover();

		this.subscribedStreamlets = checkNotNull(subscribedStreamlets);
		this.keraProperties = checkNotNull(keraProperties);

		this.keraBindings = new KeraBindingsSingleThread(keraProperties.getProperty("locator"),
				keraProperties.getProperty("clusterName"), this.handover);
		this.consumerThread = new KeraHandoverPullSingleThread(keraBindings, this.subscribedStreamlets, this.keraProperties);
	}

	@Override
	public void runFetchLoop() throws Exception {
		try {
			final Handover handover = this.handover;

			consumerThread.start();

			while (running) {
				ConsumerRecords<byte[], byte[]> records = handover.pollNext();

				//for each streamlet, for each potential record, emit
				for (Streamlet s : this.subscribedStreamlets) {
					for (ConsumerRecord<byte[], byte[]> rec : records.records(s)) {
						final T value = this.deserializer.deserialize(
							new org.apache.kafka.clients.consumer.ConsumerRecord<byte[], byte[]>("topic", 1, 0L, rec.key(), rec.value())
						);

						/*
						if (deserializer.isEndOfStream(value)) {
							// end of stream signaled
							running = false;
							break;
						}
						*/
						emitRecord(value);
					}
				}
			}

		} finally {
			this.handover.wakeupProducer();
			this.handover.close();
		}
		// on a clean exit, wait for the runner thread
		try {
			consumerThread.join();
		} catch (InterruptedException e) {
			// may be the result of a wake-up interruption after an exception.
			// we ignore this here and only restore the interruption state
			Thread.currentThread().interrupt();
		}
	}

	@Override
	public void cancel() {
		running = false;
		this.handover.wakeupProducer();
		this.handover.close();

	}

	public static <T> T checkNotNull(T reference) {
		if (reference == null) {
			throw new NullPointerException();
		}
		return reference;
	}
}
